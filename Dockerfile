FROM python:3.11
WORKDIR /app
COPY ./requirements.txt requirements.txt
COPY ./app.py .
RUN pip install --no-cache-dir --upgrade -r requirements.txt
EXPOSE 5000
CMD ["flask", "run", "--host", "0.0.0.0"]
